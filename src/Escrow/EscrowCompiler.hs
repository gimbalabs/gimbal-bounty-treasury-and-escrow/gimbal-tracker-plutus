{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module Escrow.EscrowCompiler where

import Cardano.Api
import Cardano.Api.Shelley (PlutusScript (..))
import Codec.Serialise (serialise)
import Data.Aeson
import qualified Data.ByteString.Lazy as LBS
import qualified Data.ByteString.Short as SBS
import qualified Ledger

import Plutus.V1.Ledger.Api (Data (B, Constr, I, List, Map), ToData, toData)

import Escrow.BountyEscrow
import Escrow.BountyTreasury
import Escrow.BountyTypes
dataToScriptData :: Data -> ScriptData
dataToScriptData (Constr n xs) = ScriptDataConstructor n $ dataToScriptData <$> xs
dataToScriptData (I n) = ScriptDataNumber n
dataToScriptData (B b) = ScriptDataBytes b
dataToScriptData (Map xs) = ScriptDataMap [(dataToScriptData k, dataToScriptData v) | (k, v) <- xs]
dataToScriptData (List xs) = ScriptDataList $ fmap dataToScriptData xs

writeJson :: ToData a => FilePath -> a -> IO ()
writeJson file = LBS.writeFile file . encode . scriptDataToJson ScriptDataJsonDetailedSchema . dataToScriptData . toData

writeValidator :: FilePath -> Ledger.Validator -> IO (Either (FileError ()) ())
writeValidator file = writeFileTextEnvelope @(PlutusScript PlutusScriptV1) file Nothing . PlutusScriptSerialised . SBS.toShort . LBS.toStrict . serialise . Ledger.unValidatorScript

-- writeMyFirstValidatorScript :: IO (Either (FileError ()) ())
-- writeMyFirstValidatorScript = writeValidator "output/bounty-v0.plutus" Escrow.BountyEscrowDraft.validator

writeBountyEscrowScript :: IO (Either (FileError ()) ())
writeBountyEscrowScript = writeValidator "testnet-v3/testnet-gimbal-bounty-escrow-v2.plutus" $ Escrow.BountyEscrow.validator $ BountyParam
    {
      bountyTokenPolicyId = "6c57132fde399c9ea6e462c4214d164984891087922b6fa2472b175b"
    , bountyTokenName     = "tpblTestGimbal"
    , accessTokenPolicyId = "5d6b6c332866044b2a8bdd147d92f77e42714986f1cb98cef70e201f"
    , treasuryIssuerPkh   = "957aaaf3b663749e6fa6d57137bddf03acb5206320a84c17df12c7e4"
    }

writeBountyTreasuryScript :: IO (Either (FileError ()) ())
writeBountyTreasuryScript = writeValidator "testnet-v3/testnet-gimbal-bounty-treasury-v2.plutus" $ Escrow.BountyTreasury.validator $ TreasuryParam
    {
      atSymbol            = "5d6b6c332866044b2a8bdd147d92f77e42714986f1cb98cef70e201f"
    , bountyContract      = "c903a00e869950042b89b028be999414112f7c3359d4e3a9f6f953c9"
    , bSymbol             = "6c57132fde399c9ea6e462c4214d164984891087922b6fa2472b175b"
    , bName               = "tpblTestGimbal"
    , tIssuerPkh          = "957aaaf3b663749e6fa6d57137bddf03acb5206320a84c17df12c7e4"
    }