{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# OPTIONS_GHC -fno-warn-unused-imports #-}

module Escrow.BountyTreasury where

import              Ledger              hiding (singleton)
import              Ledger.Typed.Scripts
import              Ledger.Value        as Value
import              Ledger.Ada
import qualified    PlutusTx
import              PlutusTx.Prelude    hiding (Semigroup (..), unless)
import              Prelude             (Show (..))
import Ledger.Address
import Escrow.BountyTypes
-- V2: Add Issuer's PKH as treasury Param
-- The treasuryIssuerPkh is included here to simplify initial implementation of Deposit and Withdraw actions

-- Use Redeemer to submit a "request" to Treasury for x lovelace and y gimbals
-- Validator will check that datum is being created to match request

{-# INLINEABLE mkValidator #-}
mkValidator :: TreasuryParam -> WithdrawalDatum -> BountyDetails -> ScriptContext -> Bool
mkValidator tp dat b ctx =    traceIfFalse "Only Issuer can change Treasury"                signedByIssuer ||

                              traceIfFalse "Access token missing from input"              inputHasAuthToken &&
                              traceIfFalse "Access token missing from contract output"    contractOutputHasAuthToken &&
                              traceIfFalse "Output Value must match BountyDetails"        checkValueToBountyContract &&
                              traceIfFalse "Treasury must keep remaining lovelace"        treasuryGetsLovelaceBack &&
                              traceIfFalse "Treasury must keep remaining tokens"          treasuryGetsTokensBack &&
                              traceIfFalse "You gotta add DATUM to that contract UTXO"    checkContractOutputHasDatum
                              -- checkReIsOutDat currently results in a Validation error: Why, and how can we fix it?
                              -- && traceIfFalse "redeemer is not datum" checkReIsOutDat


  -- Q: How can we check that the Bounty UTXO has the *correct* Datum?
  -- 1. First, we confirm that the Output Value to the Contract matches BountyDetails in Redeemer
  -- 2. Then, we confirm that the the new Bounty UTXO has datumhash to match those same BountyDetails

  -- symbols :: Value -> [CurrencySymbol]
  -- valueSpent :: TxInfo -> Value

  where
    info :: TxInfo
    info = scriptContextTxInfo ctx

    signedByIssuer :: Bool
    signedByIssuer = txSignedBy info $ tIssuerPkh tp

    -- Create a list of all CurrencySymbol in tx input
    inVals :: [CurrencySymbol]
    inVals = symbols $ valueSpent info

    -- Check that list of CurrencySymbols includes Auth CurrencySymbol
    inputHasAuthToken :: Bool
    inputHasAuthToken = (atSymbol tp) `elem` inVals

    -- The Value to be included in Bounty Contract UTXO
    toBountyContract :: Value
    toBountyContract = valueLockedBy info (bountyContract tp)

    -- Check that the Auth Token is sent to Bounty Contract UTXO
    contractOutputHasAuthToken :: Bool
    contractOutputHasAuthToken = (atSymbol tp) `elem` (symbols toBountyContract)

    -- Check that the Value sent to Contract UTXO matches what is specified in the Redeemer
    -- Note: For now, we can just remember to match Treasury Redeemer to Bounty Datum
    -- when we build transactions
    -- See below for extension taks
    checkValueToBountyContract :: Bool
    checkValueToBountyContract =  (getLovelace $ fromValue toBountyContract) >= (lovelaceAmount b) &&
                                  (valueOf toBountyContract (bSymbol tp) (bName tp)) >= (tokenAmount b)

    -- The UTXO input from Treasury
    ownInput :: TxOut
    ownInput = case findOwnInput ctx of
        Nothing -> traceError "treasury input missing"
        Just i  -> txInInfoResolved i

    -- The UTXO output back to Treasury
    ownOutput :: TxOut
    ownOutput = case getContinuingOutputs ctx of
        [o] -> o -- There must be exactly ONE output UTXO
        _   -> traceError "expected exactly one treasury output"

    -- INVESTIGATE: Do we need guards for including Datum on ownOutput, or is that built in to Plutus/cli?

    -- Values of each
    treasuryInputValue :: Value
    treasuryInputValue = txOutValue ownInput

    treasuryOutputValue :: Value
    treasuryOutputValue = txOutValue ownOutput

    -- Compare Values from and to Treasury to make sure that Treasury gets the right value back.
    treasuryGetsLovelaceBack :: Bool
    treasuryGetsLovelaceBack = (getLovelace $ fromValue treasuryInputValue) - (getLovelace $ fromValue treasuryOutputValue) <= (getLovelace $ fromValue toBountyContract)

    treasuryGetsTokensBack :: Bool
    treasuryGetsTokensBack = (valueOf treasuryInputValue (bSymbol tp) (bName tp)) - (valueOf treasuryOutputValue (bSymbol tp) (bName tp)) <= (valueOf toBountyContract (bSymbol tp) (bName tp))

    -- It looks like this might be redundant because of how transactions validate
    -- But I'm not sure. Create a Bounty to test this.
    checkContractOutputHasDatum :: Bool
    checkContractOutputHasDatum = case scriptOutputsAt (bountyContract tp) info of
      [(d, _)]  -> True
      _         -> False

      -- txOutDatum :: TxOut -> Maybe DatumHash

    -- NEXT TASK (PPBL / Technical Group)
    -- Compare "incoming" Redeemer to "outgoing" Datum
    -- The following code compiles, but the logic does not quite work.
    -- Figure out why not / how to fix
    -- Helpful: https://github.com/input-output-hk/plutus-pioneer-program/blob/main/code/week07/src/Week07/EvenOdd.hs
    -- -- txDatumMatchesRedeemer -----------------------------------------------------------------------------------------------
    -- -- From Redeemer to BuiltinData
    -- redData :: BuiltinData
    -- redData = PlutusTx.toBuiltinData b

    -- -- And from there, to Datum
    -- redDatum :: Maybe Datum
    -- redDatum = case PlutusTx.fromBuiltinData redData of
    --     Just d  -> Just d
    --     Nothing -> traceError "cannot make datum"

    -- -- Which allows us to look for the match:
    -- getMatchingDatum :: Maybe DatumHash
    -- getMatchingDatum = case redDatum of
    --   Nothing   -> traceError "do not see that datum"
    --   Just d    -> findDatumHash d info

    -- -- And finally return True if there's a match!
    -- txDatumMatchesRedeemer :: Bool
    -- txDatumMatchesRedeemer = case getMatchingDatum of
    --   Nothing   -> False
    --   Just _    -> True

    getEscrowDatum :: Maybe BountyEscrowDatum
    getEscrowDatum = let os = [ o | o <- txInfoOutputs info, txOutValue o == toBountyContract ] in
                  case os of
                    [o] -> escrowDatum o (`findDatum` info)
                    _   -> Nothing


    checkReIsOutDat :: Bool
    checkReIsOutDat = case getEscrowDatum of
      Nothing -> False
      Just ns -> bedIssuerPkh   ns == issuerPkh b &&
                 bedContributorPkh ns == contributorPkh b &&
                 bedLovelaceAmount ns == lovelaceAmount b &&
                 bedTokenAmount ns == tokenAmount b &&
                 bedExpirationTime ns == expirationTime b



typedValidator :: TreasuryParam -> TypedValidator TreasuryTypes
typedValidator tp =
  mkTypedValidator @TreasuryTypes
    ($$(PlutusTx.compile [||mkValidator||]) `PlutusTx.applyCode` PlutusTx.liftCode tp)
    $$(PlutusTx.compile [||wrap||])
  where
    wrap = wrapValidator @WithdrawalDatum @BountyDetails

validator :: TreasuryParam -> Validator
validator = validatorScript . typedValidator

treasuryValidatorHash :: TreasuryParam -> ValidatorHash
treasuryValidatorHash = Ledger.validatorHash . validator
