{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Escrow.BountyTrace
    ( test
    , lockTreasuryTest
    , issuer 
    , contributor1
    , updateTest
    , cancelTest
    , contributeBountyTest
    , distributeTest
    ) where

import           Plutus.Trace.Emulator          as Emulator
                                                (activateContractWallet, waitNSlots, runEmulatorTraceIO', callEndpoint, EmulatorConfig(..))
import           Control.Monad                  (void)
import           PlutusTx.Prelude               as Plutus (($), Either(..), (<>), Integer)
import           Ledger.Value                   as Value (singleton, CurrencySymbol (CurrencySymbol))
import qualified Data.Map                       as Map
import qualified Ledger.Ada                     as Ada
import           Prelude                        (IO)
import           Data.Default                   (def)
import           Plutus.V1.Ledger.Value         (Value (..), TokenName (..))
import           Wallet.Emulator.Wallet         (Wallet (..), knownWallet, mockWalletPaymentPubKeyHash)
import Ledger (PaymentPubKeyHash(), POSIXTime)
import Ledger.TimeSlot (slotToEndPOSIXTime)
 
import Escrow.BountyOffchain as BountyOffchain

issuer :: Wallet 
issuer = knownWallet 1

contributor1 :: Wallet 
contributor1 = knownWallet 2

contributor2 :: Wallet 
contributor2 = knownWallet 3

walletPpkh :: Wallet -> PaymentPubKeyHash 
walletPpkh = mockWalletPaymentPubKeyHash 

-- initial wallet balance
dist :: Map.Map Wallet Value 
dist = Map.fromList [ (issuer, Ada.lovelaceValueOf 100_000_000 
                             <> Value.singleton bountyCs bountyTn 1000 
                             -- for identification
                             <> Value.singleton (CurrencySymbol "a") (TokenName "issuer") 1)
                            , (contributor1, Ada.lovelaceValueOf 100_000_000
                             <> Value.singleton authTokenCS authTokenTn 1
                             <> Value.singleton (CurrencySymbol "a") (TokenName "contributor1") 1)
                            , (contributor2, Ada.lovelaceValueOf 100_000_000
                             <> Value.singleton authTokenCS authTokenTn 1
                             <> Value.singleton (CurrencySymbol "a") (TokenName "contributor2") 1)
                                                            ]
     
emCfg :: EmulatorConfig
emCfg = EmulatorConfig (Left dist) def def 


-- data we need to build validator, redeemers and datums 
authTokenCS :: CurrencySymbol 
authTokenCS = CurrencySymbol "auth"

authTokenTn :: TokenName 
authTokenTn = TokenName "AuthToken"

bountyCs :: CurrencySymbol 
bountyCs = CurrencySymbol "bounty" 

bountyTn :: TokenName 
bountyTn = TokenName "gimbals"

testTime :: POSIXTime
testTime = slotToEndPOSIXTime def 10 

contributeBountyAm :: Integer 
contributeBountyAm = 50 

lockAtTreasuryBAm :: Integer 
lockAtTreasuryBAm = 400

lockAtTreasuryAAm :: Integer 
lockAtTreasuryAAm = 80_000_000

{-
data LockTreasuryParams = LockTreasuryParams
    { ltpBountyCs :: CurrencySymbol 
    , ltpBountyTn :: TokenName
    , ltpBountyAmount :: Integer
    , ltpGimbalsAm :: Integer
    , ltpAdaAm :: Integer
    , ltpAuthCs :: CurrencySymbol
    } deriving (Pr.Eq, Pr.Ord, Show, Generic, ToJSON, FromJSON, ToSchema)

-}

lockTreasuryParams :: BountyOffchain.LockTreasuryParams 
lockTreasuryParams = BountyOffchain.LockTreasuryParams bountyCs bountyTn contributeBountyAm lockAtTreasuryBAm lockAtTreasuryAAm authTokenCS

{-
data UpdateTreasuryParams = UpdateTreasuryParams
    { utpBountyCs :: CurrencySymbol 
    , utpBountyTn :: TokenName
    , utpBountyAmount :: Integer
    , utpAuthCs :: CurrencySymbol
    , utpAuthTn :: TokenName
    , utpIssuerPkh :: PaymentPubKeyHash 
    , utpContributorPkh :: PaymentPubKeyHash 
    , utpExpirationTime :: POSIXTime 
    , utpLovelaceAmount :: Integer
    } deriving (Pr.Eq, Pr.Ord, Show, Generic, ToJSON, FromJSON, ToSchema)


-}

utp :: BountyOffchain.UpdateTreasuryParams 
utp = BountyOffchain.UpdateTreasuryParams bountyCs bountyTn contributeBountyAm authTokenCS authTokenTn (walletPpkh issuer) (walletPpkh contributor1) testTime 5000000 

utpU :: BountyOffchain.UpdateTreasuryParams 
utpU = BountyOffchain.UpdateTreasuryParams bountyCs bountyTn 55 authTokenCS authTokenTn (walletPpkh issuer) (walletPpkh contributor1) testTime 5000000 


test :: IO ()
test = 
    runEmulatorTraceIO' def emCfg $ do
        ih1 <- activateContractWallet issuer BountyOffchain.endpoints
        ch1 <- activateContractWallet contributor1 BountyOffchain.endpoints 
        ch2 <- activateContractWallet contributor2 BountyOffchain.endpoints 
        void $ Emulator.waitNSlots 1
        callEndpoint @"lockFundsTreasury" ih1 lockTreasuryParams
        void $ Emulator.waitNSlots 1
        callEndpoint @"contributeBounty" ch1 utp
        void $ Emulator.waitNSlots 1
        callEndpoint @"updateBounty" ih1 utp
        void $ Emulator.waitNSlots 1
        callEndpoint @"contributeBounty" ch2 utpU
        void $ Emulator.waitNSlots 1


contributeBountyTest :: IO ()
contributeBountyTest = 
    runEmulatorTraceIO' def emCfg $ do 
        ih1 <- activateContractWallet issuer BountyOffchain.endpoints 
        ch1 <- activateContractWallet contributor1 BountyOffchain.endpoints 
        void $ Emulator.waitNSlots 1
        callEndpoint @"lockFundsTreasury" ih1 lockTreasuryParams
        void $ Emulator.waitNSlots 1
        callEndpoint @"contributeBounty" ch1 utp
        void $ Emulator.waitNSlots 1


updateTest :: IO ()
updateTest = 
    runEmulatorTraceIO' def emCfg $ do 
        ih1 <- activateContractWallet issuer BountyOffchain.endpoints 
        ch1 <- activateContractWallet contributor1 BountyOffchain.endpoints 
        ch2 <- activateContractWallet contributor2 BountyOffchain.endpoints 
        void $ Emulator.waitNSlots 1
        callEndpoint @"lockFundsTreasury" ih1 lockTreasuryParams
        void $ Emulator.waitNSlots 1
        callEndpoint @"contributeBounty" ch1 utp
        void $ Emulator.waitNSlots 10
        callEndpoint @"updateBounty" ih1 utp
        void $ Emulator.waitNSlots 1


cancelTest :: IO ()
cancelTest = 
    runEmulatorTraceIO' def emCfg $ do 
        ih1 <- activateContractWallet issuer BountyOffchain.endpoints 
        ch1 <- activateContractWallet contributor1 BountyOffchain.endpoints 
        void $ Emulator.waitNSlots 1
        callEndpoint @"lockFundsTreasury" ih1 lockTreasuryParams
        void $ Emulator.waitNSlots 1
        callEndpoint @"contributeBounty" ch1 utp
        void $ Emulator.waitNSlots 10
        callEndpoint @"cancelBounty" ih1 utp
        void $ Emulator.waitNSlots 1


distributeTest :: IO ()
distributeTest = 
    runEmulatorTraceIO' def emCfg $ do 
        ih1 <- activateContractWallet issuer BountyOffchain.endpoints 
        ch1 <- activateContractWallet contributor1 BountyOffchain.endpoints 
        void $ Emulator.waitNSlots 1
        callEndpoint @"lockFundsTreasury" ih1 lockTreasuryParams
        void $ Emulator.waitNSlots 1
        callEndpoint @"contributeBounty" ch1 utp
        void $ Emulator.waitNSlots 10
        callEndpoint @"distributeBounty" ih1 utp
        void $ Emulator.waitNSlots 1


lockTreasuryTest :: IO ()
lockTreasuryTest = 
    runEmulatorTraceIO' def emCfg $ do
        issuer1 <- activateContractWallet issuer BountyOffchain.endpoints
        void $ Emulator.waitNSlots 1
        callEndpoint @"lockFundsTreasury" issuer1 lockTreasuryParams
        void $ Emulator.waitNSlots 1
