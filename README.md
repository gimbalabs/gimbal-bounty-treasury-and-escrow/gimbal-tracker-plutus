# Gimbalabs Escrow


## Description
This package contains Plutus source code for Gimbal Bounty and Escrow Project.
## Installation
To set up development environment in which you can compile this code, follow this guide: <br />
[https://gitlab.com/gimbalabs/ppbl-course-01/ppbl-course-01/-/tree/master/project-01](https://gitlab.com/gimbalabs/ppbl-course-01/ppbl-course-01/-/tree/master/project-01) <br />
Once you have the development environment, to compile the script follow the procedure described in ppbl-course-01 (above).
## Usage
- This package integrates with [Gimbal Tracker V1 Front End](https://gitlab.com/gimbalabs/gimbal-bounty-treasury-and-escrow/gimbal-tracker-v1).
- Guide pending.
- The Front End repo has separate git branches for `mainnet`, `testnet`, and may in the future include staging branches for other deployments.
- In this Plutus repo, there is one `master` branch. Compile Contracts with other parameters, or for testnet/mainnet by adding a folder like `/mainnet` and editing `src/Escrow/EscrowCompiler`

## Support
contact us at: [https://discord.gg/x8xw3B5jCm](https://discord.gg/x8xw3B5jCm)

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing

## Authors and acknowledgment
James, Genty, MIxAxIM, Piotr, Hari, Adrian, Abdelkrim, Alex

## License

## Project status
