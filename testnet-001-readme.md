# Testing Bounty Escrow Contract

Public Testnet Version 1.0

## Project Variables:
- The Bounty Payment token is: `tpblTestGimbal`: `6c57132fde399c9ea6e462c4214d164984891087922b6fa2472b175b` or `6c57132fde399c9ea6e462c4214d164984891087922b6fa2472b175b.7470626c5465737447696d62616c`
- In this instance, the Access Token PolicyID is: `5d6b6c332866044b2a8bdd147d92f77e42714986f1cb98cef70e201f`
- Bounty Treasury: `addr_test1wql5wkfvdv0t605pwgaqj6zzpj65n50t8cxh5g5rapvrkhcvxr229`
- Bounty Escrow: `addr_test1wryms9twwky535d2d8y9tkqke9lrlhg7hlm9dvnf2uk7tpcp07v2g`
- Bounty Escrow Validator Hash: `c9b8156e758948d1aa69c855d816c97e3fdd1ebff656b269572de587`

## 0. Overview

### Roles:
- *Issuer* creates Bounties and deposits tokens in Treasury
- *Contributor* commits to Bounties and received tokens upon completion

### Contracts
1. Treasury: should always contain exactly one UTXO
2. Bounty: contains one UTXO for EACH BOUNTY

### Interactions
1. (On-Chain) Issuer deposits bounty funds at Treasury Contract
2. (Off-Chain) Issuer drafts bounties and places on Front End (`.md` files)
3. Contributor commits to bounties on Front End, which creates a transaction that UNLOCKS TREASURY funds and LOCKS BOUNTY UTXO:
  a. TXIN from Treasury Contract and BOUNTY TXOUT with value equal to Bounty Params specified on Front End (this is not secure, but relying on trusted use of Auth token allows us to create this MVP)
  b. Creates Datum to represent Bounty (see `BountyEscrowDraft.hs`)
  c. Deposits Bounty UTXO at Bounty Contract
4. Only the ISSUER can UNLOCK BOUNTY UTXOs ("good faith" MVP):
  a. Distribute to Contributor
  b. Update (by raising Bounty Value or extending Expiration)
  c. Cancel Bounty, after some deadline is reached

## 1. Get/Create Tokens

### Official Testnet Tokens:
- The Bounty Payment token is: `tpblTestGimbal`: `6c57132fde399c9ea6e462c4214d164984891087922b6fa2472b175b` or `6c57132fde399c9ea6e462c4214d164984891087922b6fa2472b175b.7470626c5465737447696d62616c`

### To mint "Access Tokens":
- In this instance, the Access Token PolicyID is: `5d6b6c332866044b2a8bdd147d92f77e42714986f1cb98cef70e201f`
- PolicyID was created using this Testnet Address: "addr_test1qq3pzlaap7r2yyawfaxcynxs6w8w520yjaj2ugh4759685cv5qhzt95z9t8lur483fur90ge4ppqk2j89gmu8yy9m0ksac989n"

`accessPolicy.script`:

```
{
    "keyHash": "22117fbd0f86a213ae4f4d824cd0d38eea29e49764ae22f5f50ba3d3",
    "type": "sig"
}
```

## 2. Use the PolicyID's and Bounty Token name defined in Step (1) to change parameters in `/gimbal-tracker-plutus/src/EscrowCompiler.hs`

Note: you might see the words "PolicyID", "policy", and "CurrencySymbol" used interchangeably.

### 2a. Compile Bounty Contract (needs detailed documentation / links)
- This is a parameterized validator: must define the bounty payment token

### 2b. Build Bounty Address
```
cardano-cli address build --payment-script-file testnet-gimbal-bounty-escrow-v1.plutus --testnet-magic 1097911063 --out-file testnet-gimbal-bounty-escrow-v1.addr
```
- Try looking up this new Address on [cardanoscan testnet](https://testnet.cardanoscan.io/)

### 2c. Get Validator Hash of Bounty Contract (needs detailed documentation / links)
- To Create a Validator Hash, we must use `cardano-cli transaction policyid`

### 2d. Use Bounty Contract Validator Hash to Compile Treasury Contract (needs detailed documentation / links)

### 2e. Build Treasury Address
```
cardano-cli address build --payment-script-file bounty-play-testnet.plutus --testnet-magic 1097911063 --out-file bounty-play-testnet.addr
```

### When this step is complete, you'll have new Contract Addresses. Note them here:
- Bounty Treasury: "addr_test1wql5wkfvdv0t605pwgaqj6zzpj65n50t8cxh5g5rapvrkhcvxr229"
- Bounty Escrow: "addr_test1wryms9twwky535d2d8y9tkqke9lrlhg7hlm9dvnf2uk7tpcp07v2g"
- Bounty Escrow Validator Hash: "c9b8156e758948d1aa69c855d816c97e3fdd1ebff656b269572de587"

## 3. Learn to Use by building Transactions on `cardano-cli`:
In deployment, [gimbal-tracker-frontend](https://gitlab.com/gimbalabs/gimbal-bounty-treasury-and-escrow/gimbal-tracker-v1) builds transactions that are validated by the Contract. For practice, you can learn how these contracts work by building transactions with `cardano-cli`. You will need access to a synced testnet node.

### We can organize this work into three transactions
1. Issuer locks funds at Treasury Contract
2. Contributor commits to Bounty, which unlocks Treasury funds and locks a new UTXO at Bounty Contract
3. Issuer distributes Bounty UTXO upon completion

Let's take a look at each:

## Transaction #1: Issuer Locks Funds at Treasury Contract:
- Requires Datum:
```
To Do
```

- Set Variables:
```
SENDER
SENDERKEY
TXIN=
TXIN2=
TREASURY=
```

- Build, Sign and Submit Tx:
```
cardano-cli transaction build \
--alonzo-era \
--tx-in $TXIN1 \
--tx-in $TXIN2 \
--tx-out $TREASURY+"99000000 + 400 cef5bfce1ff3fc5b128296dd0aa87e075a8ee8833057230c192c4059.706c6179" \
--tx-out-datum-embed-file treasury-withdrawal-datum.json \
--tx-out $SENDER+"2000000 + 11200 cef5bfce1ff3fc5b128296dd0aa87e075a8ee8833057230c192c4059.706c6179" \
--change-address $SENDER \
--protocol-params-file protocol.json \
--out-file tx.raw \
--testnet-magic 1097911063

cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
--testnet-magic 1097911063 \
--tx-body-file tx.raw \
--out-file tx.signed

cardano-cli transaction submit \
--tx-file tx.signed \
--testnet-magic 1097911063
```

---

## Transaction #2. Contributor commits to Bounty, which unlocks Treasury funds and locks a new UTXO at Bounty Contract
- Requires incoming Datum: treasury-withdrawal-datum.json

- Requires Redeemer
```
data BountyEscrowDatum = BountyEscrowDatum
  {
    issuerAddress       :: !PubKeyHash
  , contributorAddress  :: !PubKeyHash
  , lovelaceAmount      :: !Integer
  , tokenAmount         :: !Integer
  , expirationTime      :: !POSIXTime
  }

{"constructor":0,"fields":[{"bytes":"22117fbd0f86a213ae4f4d824cd0d38eea29e49764ae22f5f50ba3d3"},{"bytes":"358b2dbffc561e85c8251af0b79534be44fdf049e66ef3e2f7aa9418"},{"int":12000000},{"int":500},{"int":1649778363}]}
```

- Requires outgoing Datum, to create Bounty - should match Redeemer, just paying attention to where

- In `bounty-play-testnet.plutus`, we use custom Types for Datum and Redeemer. (see `/Escrow/BountyEscrowDraft.hs`)
- Create examples for each


- Issuer: Use pkh `22117fbd0f86a213ae4f4d824cd0d38eea29e49764ae22f5f50ba3d3` from test-wallet
- Contributor: need a pkh
- 1649778363 posixtime 2022-04-11 update??


```
cardano-cli transaction hash-script-data --script-data-file ....
> result
```

### Set Variables:
```
TRESTXIN=
CONTRIBTXIN=
```

### Bug Bounty: How can you drain the treasury?

## Notes + Next Steps
- TODO: There is not yet any logic to protect this TX from Validating without tx-out-datum on Treasury Output.
- However, when datum is removed from BOUNTY output, it looks like the contract doesn't even "see" that output as a contract output. Investigate this.

#### Set Variables
```
TRESTXIN=#1
TREASURYPLUTUSSCRIPT=
TREASURYDATUM=
TREASURYREDEEMER=
CONTRIBTXIN=
COLLATERAL=
BOUNTYCONTRACTADDR=
AUTHTOKENASSET=
PAYMENTASSET=
CONTRIBUTORADDR=
```


```
cardano-cli transaction build \
--alonzo-era \

--tx-in $TRESTXIN \
--tx-in-script-file bounty-treasury.plutus \
--tx-in-datum-file treasury-withdrawal-datum.json \
--tx-in-redeemer-file datum-play-test3.json \

--tx-in $CONTRIBTXIN \

--tx-in-collateral $COLLATERAL \

--tx-out $BOUNTYCONTRACTADDR+"5000000 + 1 $AUTHTOKENASSET + 40 cef5bfce1ff3fc5b128296dd0aa87e075a8ee8833057230c192c4059.706c6179" \
--tx-out-datum-embed-file datum-play-test3.json \

--tx-out $TREASURY+"90000000 + 560 cef5bfce1ff3fc5b128296dd0aa87e075a8ee8833057230c192c4059.706c6179" \
--tx-out-datum-hash "da60dd5a402c87e266c975c25721af9d86196f78416e781d36b5e9bad78ce1fa" \

--change-address $CONTRIBUTOR \

--protocol-params-file protocol.json \
--testnet-magic 1097911063 \
--out-file tx.raw

cardano-cli transaction sign \
--signing-key-file $CONTRIBUTORKEY \
--testnet-magic 1097911063 \
--tx-body-file tx.raw \
--out-file tx.signed

cardano-cli transaction submit \
--tx-file tx.signed \
$TESTNET
```

### Unlock Funds
```
SENDERTXIN=
CONTRACTTXIN=
```

## Here is a working UPDATE Transaction
```
cardano-cli transaction build \
--alonzo-era \
--testnet-magic 1097911063 \
--tx-in $SENDERTXIN \
--tx-in $CONTRACTTXIN \
--tx-in-script-file bounty-play-testnet.plutus \
--tx-in-datum-file datum-play-test1.json \
--tx-in-redeemer-file update.json \
--tx-in-collateral $COLLATERAL \
--tx-out $CONTRACT+"12000000 + 500 cef5bfce1ff3fc5b128296dd0aa87e075a8ee8833057230c192c4059.706c6179" \
--tx-out-datum-embed-file datum-play-test1.json \
--change-address $SENDER \
--required-signer $SENDERKEY \
--protocol-params-file protocol.json \
--out-file unlock.raw

cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
--testnet-magic 1097911063 \
--tx-body-file unlock.raw \
--out-file unlock.signed

cardano-cli transaction submit \
--tx-file unlock.signed \
--testnet-magic 1097911063
```
## Try a working DISTRIBUTE Tx
```
cardano-cli transaction build \
--alonzo-era \
--testnet-magic 1097911063 \
--tx-in $ISSUERTXIN \
--tx-in $BOUNTYTXIN \
--tx-in-script-file bounty-play-testnet.plutus \
--tx-in-datum-file datum-play-test3.json \
--tx-in-redeemer-file distribute.json \
--tx-in-collateral $COLLATERAL \
--tx-out $CONTRIBUTOR+"12000000 + 1 3794c001b97da7a47823ad27b29e049985a9a97f8aa6908429180e2c.506c7574757350424c436f757273653031 + 51 cef5bfce1ff3fc5b128296dd0aa87e075a8ee8833057230c192c4059.706c6179" \
--change-address $SENDER \
--required-signer $SENDERKEY \
--protocol-params-file protocol.json \
--out-file unlock.raw

cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
--testnet-magic 1097911063 \
--tx-body-file unlock.raw \
--out-file unlock.signed

cardano-cli transaction submit \
--tx-file unlock.signed \
--testnet-magic 1097911063
```
