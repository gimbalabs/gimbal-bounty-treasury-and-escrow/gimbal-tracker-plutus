## Instance Variables:

### Issuer (needs Nami access, for now):
- address: "addr1qy3xh5h4gjt6z69jqvrt04l0g2d82h4s6cf0xvu7t2nz95j6jnltpt22t0myhsuk9s6lyn74yxmlxakt27ylr9ykh30qz97lf8"
- pkh: ""

### bounty token:
- policy id: "2b0a04a7b60132b1805b296c7fcb3b217ff14413991bf76f72663c30"
- name: "gimbal"

### access token (minted by Monday)
- simple minting requires a pkh
- I am using `cardano-cli` to mint simple access tokens.
- policy id: "68ae22138b3c82c717713d850e5ee57c7de5de8591f5f13cd3a6cc67"
- Access token issuer should keep track of naming conventions

#### To mint a simple access token, create a simple Mary-era policy.
- Mainnet Address: "addr1q9t44xsq6uyr0ds5vv23x92q7zzgph4kny3q758zndgt7c9a4nqn5t5w9e36hps534yv9uejam2snhgkup5rvqm74xjsx46smn"

`accessPolicy.script`:

```
{
    "keyHash": "575a9a00d70837b6146315131540f08480deb699220f50e29b50bf60",
    "type": "sig"
}
```

### Contract Addresses:
- Bounty Treasury: "addr1w8xx7afyez9ng0pa77eqlfkn5qwf4kkdzt5zyhnajdyxetsxpk49z"
- Bounty Escrow: "addr1w8cfe0dxfa6zfthly4rgf7zsdkyp4zmtm5nk8ghksgfcc9c5nmdkn"
- Bounty Escrow Validator Hash: "f09cbda64f7424aeff254684f8506d881a8b6bdd2763a2f682138c17"
